import argparse, textwrap
from tic_tac_toe.main import main
import tic_tac_toe.basic_ais as basic_ais
import tic_tac_toe.minimax_ai as minimax_ai


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description=textwrap.dedent(
            """\
            Difficulty Options for the AI
            -------------------------
            0 Human Player
            1 Easy
            2 Normal
            3 Expert
        """
        ),
        formatter_class=argparse.RawDescriptionHelpFormatter,
    )
    parser.add_argument("player1", type=int, help="Choice for player 1")
    parser.add_argument("player2", type=int, help="Choice for player 2")

    args = parser.parse_args()
    args = [args.player1, args.player2]

    for arg in args:
        if arg == 0:
            args[args.index(arg)] = basic_ais.human_player
        elif arg == 1:
            args[args.index(arg)] = basic_ais.random_ai
        elif arg == 2:
            args[args.index(arg)] = basic_ais.finds_winning_and_losing_moves_ai
        elif arg == 4:
            args[args.index(arg)] = minimax_ai.minimax_ai
        elif arg == 3:
            args[args.index(arg)] = minimax_ai.minimax_ai_with_cache

    main(args[0], args[1])
